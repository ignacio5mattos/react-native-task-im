import { StyleSheet } from "react-native";

const colors = {
  error: "red",
  primary: "#380CA4",
  secondary: "#01EE4F",
  white: "#f8f9fa",
};

const styles = StyleSheet.create({
  backgroundImage: {
    width: "100%",
    height: "100%",
  },
  container: {
    padding: 25,
  },
});

export { styles, colors };
