import { StyleSheet } from "react-native";
import { colors } from "../../styles/global.styles";

const styles = StyleSheet.create({
  container: {
    height: "100%",
    padding: 25,
    alignItems: "center",
    justifyContent: "center",
  },
});

export default styles;
